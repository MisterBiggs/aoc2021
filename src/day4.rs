use std::fs;

pub fn run() {
    println!("Day 4:");
    let input = fs::read_to_string("./inputs/day4.txt").expect("Could not read file");

    let numbers: Vec<&str> = input.split('\n').next().unwrap().split(',').collect();
    for i in numbers.iter() {
        println!("\t{}", i);
    }

    let boards: Vec<&str> = input.split("\n\n").collect();
    println!("{}", boards.len());
    for i in boards.iter() {
        println!("\t{:?}", i);
    }
}
