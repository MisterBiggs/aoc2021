use std::fs;

pub fn run() {
    println!("Day 2:");
    let input = fs::read_to_string("./inputs/day2.txt").expect("Could not read file");

    let commands: Vec<(&str, i32)> = input
        .lines()
        .map(|s| {
            let mut split = s.split_whitespace();
            (
                split.next().unwrap(),
                split.next().unwrap().parse::<i32>().unwrap(),
            )
        })
        .collect();

    part1(&commands);
    part2(&commands);
}

fn part1(commands: &Vec<(&str, i32)>) {
    let mut position = 0;
    let mut depth = 0;

    for (dir, amt) in commands.iter().map(|a| *a) {
        let change = match dir {
            "forward" => (amt, 0),
            "up" => (0, -amt),
            "down" => (0, amt),
            _ => (0, 0),
        };

        position += change.0;
        depth += change.1;
    }
    println!("\tPart 1: {}", position * depth);
}

fn part2(commands: &Vec<(&str, i32)>) {
    let mut position = 0;
    let mut depth = 0;
    let mut aim = 0;

    for (dir, amt) in commands.iter().map(|a| *a) {
        let change = match dir {
            "forward" => (amt, 0),
            "up" => (0, -amt),
            "down" => (0, amt),
            _ => (0, 0),
        };

        aim += change.1;
        position += change.0;
        depth += aim * change.0;
    }
    println!("\tPart 2: {}", position * depth);
}
