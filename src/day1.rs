use std::fs;

pub fn run() {
    println!("Day 1:");
    let input = fs::read_to_string("./inputs/day1.txt").expect("Could not read file");

    let lines: Vec<i32> = input.lines().map(|s| s.parse().unwrap()).collect();

    part1(&lines);
    part2(&lines);
}

fn part1(lines: &Vec<i32>) {
    let mut previous: i32 = 10000;
    let mut increases: i32 = 0;

    for current in lines.iter().map(|a| *a) {
        if previous < current {
            increases += 1;
        }
        previous = current;
    }

    println!("\tPart 1: {}", increases);
}

fn part2(lines: &Vec<i32>) {
    let mut previous: i32 = 10000;
    let mut increases = 0;

    for i in 0..lines.len() - 2 {
        let current = lines[i..i + 3].iter().sum();

        if previous < current {
            increases += 1;
        }
        previous = current;
    }

    println!("\tPart 2: {}", increases);
}
