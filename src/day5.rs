use itertools::Itertools;
use std::collections::HashMap;
use std::fs;

pub fn run() {
    println!("Day 5:");
    let input = fs::read_to_string("./inputs/day5.txt").expect("Could not read file");

    let lines = input
        .lines()
        .filter_map(|l| {
            l.split(" -> ")
                .map(|s| s.split(','))
                .flatten()
                .map(|i| i.parse::<i8>().unwrap())
                .collect_tuple()
        })
        .collect::<Vec<(i8, i8, i8, i8)>>();

    let mut points = HashMap::new();

    for (x1, y1, x2, y2) in lines {
        let dx: i8 = x2 - x1;
        let dy: i8 = y2 - y1;
        for x in x1..x1 + dx {
            for y in y1..y1 + dy {
                *points.entry((x, y)).or_insert(0) += 1;
            }
        }
    }

    for (key, value) in points.into_iter() {
        println!("\t({}, {}), {}", key.0, key.1, value);
    }
}
